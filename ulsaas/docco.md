## Installation / Setup

Prerequisites

- [git]
- [node.js] (along with npm, which should get installed automatically)

Install project dependencies
```
npm install -g coffee-script
```

Pull down the project repo & install internal dependencies

```
git clone git@bitbucket.org:jsleeuw/ul-saas.git
cd ul-sass
npm install
```

Run tests
```
npm test
```

## Classes

### User

### Policy

- id: string
- type: string
- startDate: date
- premium: integer

```json
{
  id:1,
  type:"XYZ",
  startDate:1407328289,
  premium:1000
}
```


### Fund

### Fund Allocation

### Unit Holding

## API

### Policy

#### Create

```json

        method: "POST",
        url: "/policies/create",
        payload: {
          type: 'type',
          premium: 1000,
          fundAllocations: [
            {
              allocation: 100,
              fund_id: 1
            }
          ]
        }

```


### Tranche

#### Open

```json
        method: "POST",
        url: "/tranches/open",
        payload: {}
```

#### Close

```json
        method: "POST",
        url: "/tranches/close",
        payload: {
          openNew: bool
        }
```

[node.js]:http://nodejs.org
[git]:http://git-scm.com/downloads
